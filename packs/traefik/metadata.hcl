app {
  url = "https://traefik.io/"
  author = "Traefik Labs"
}

pack {
  name = "traefik"
  description = "Traefik is a modern reverse proxy and load balancer. It runs as a Nomad system job."
  url = "https://github.com/hashicorp/nomad-pack-community-registry/traefik"
  version = "0.0.1"
}
